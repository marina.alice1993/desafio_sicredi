package br.com.marina;

import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.*;

public class PostSimulacoesTest {

    @Test
    public void postSimulacoes201(){

        String url = "http://localhost:8080/api/v1/simulacoes";
        JSONObject simulacao = new JSONObject();
        String cpf = "02540405002";
        String nome = "Marina Alice";
        String email = "marina.alice1993@gmail.com";
        int parcelas = 9;
        Boolean seguro = true;

        simulacao.put("cpf", cpf);
        simulacao.put("nome", nome);
        simulacao.put("email", email);
        simulacao.put("valor", 10000);
        simulacao.put("parcelas", parcelas);
        simulacao.put("seguro", seguro);

        given().
            contentType(ContentType.JSON).
            body(simulacao.toString()).
        when().
            post(url).
        then().
            statusCode(201).
            body("nome", is(nome)).
            body("cpf", is(cpf)).
            body("email", is(email)).
            body("valor", is(10000)).
            body("parcelas", is(parcelas)).
            body("seguro", is(seguro));
    }

    @Test
    public void postSimulacoes400(){

        String url = "http://localhost:8080/api/v1/simulacoes";
        JSONObject simulacao = new JSONObject();
        String cpf = "02540405002";
        String nome = "Marina Alice";
        String email = "marina.alice1993@gmail.com";
        int parcelas = 9;
        Boolean seguro = true;

        simulacao.put("cpf", cpf);
        simulacao.put("nome", nome);
        simulacao.put("email", email);
        simulacao.put("valor", "teste");
        simulacao.put("parcelas", parcelas);
        simulacao.put("seguro", seguro);

        given().
            contentType(ContentType.JSON).
            body(simulacao.toString()).
        when().
            post(url).
        then().
            statusCode(400);
    }

    @Test
    public void postSimulacoes409(){

        String url = "http://localhost:8080/api/v1/simulacoes";
        JSONObject simulacao = new JSONObject();
        String cpf = "02540405002";
        String nome = "Marina Alice";
        String email = "marina.alice1993@gmail.com";
        int parcelas = 9;
        Boolean seguro = true;

        simulacao.put("cpf", cpf);
        simulacao.put("nome", nome);
        simulacao.put("email", email);
        simulacao.put("valor", 10000);
        simulacao.put("parcelas", parcelas);
        simulacao.put("seguro", seguro);

        given().
            contentType(ContentType.JSON).
            body(simulacao.toString()).
        when().
            post(url).
        then().
            statusCode(409).
            body("mensagem",is("CPF duplicado"));
    }
}
