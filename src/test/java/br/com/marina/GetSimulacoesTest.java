package br.com.marina;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

public class GetSimulacoesTest {

    @Test
    public void getConsultaTodas(){

        String url = "http://localhost:8080/api/v1/simulacoes";

        given().
        when().
            get(url).
        then().
            statusCode(200);
    }

    @Test
    public void getConsultaTodas204(){

        String url = "http://localhost:8080/api/v1/simulacoes";

        given().
        when().
            get(url).
        then().
            statusCode(204);
    }

    @Test
    public void getConsultaCPF(){

        String url = "http://localhost:8080/api/v1/simulacoes";
        String cpf = "02540405002";

        given().
            pathParam("cpf", cpf).
        when().
            get(url + "/{cpf}").
        then().
            statusCode(200).
            body("cpf", is(cpf));
    }

    @Test
    public void getConsultaCPF404(){

        String url = "http://localhost:8080/api/v1/simulacoes/02540405003";

        given().
        when().
            get(url).
        then().
            statusCode(404);
    }
}
