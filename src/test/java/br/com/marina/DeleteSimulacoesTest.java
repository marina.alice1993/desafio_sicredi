package br.com.marina;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

public class DeleteSimulacoesTest {

    @Test
    public void deletesimulacoes204(){

        String url = "http://localhost:8080/api/v1/simulacoes";
        String cpf = "02540405002";

        int id = given().
            pathParam("cpf", cpf).
        when().
            get(url + "/{cpf}").
        then().
        extract().
            path("id");

        given().
            pathParam("id", id).
        when().
            delete(url + "/{id}").
        then().
            statusCode(204);
    }

    @Test
    public void deletesimulacoes404(){

        String url = "http://localhost:8080/api/v1/simulacoes";
        int id = 10;

        given().
            pathParam("id", id).
        when().
            delete(url + "/{id}").
        then().
            statusCode(404).
            body("mensagem",is(" Simulação não encontrada"));
    }
}
