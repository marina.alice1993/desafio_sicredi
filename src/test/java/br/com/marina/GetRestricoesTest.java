package br.com.marina;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class GetRestricoesTest {

    @Test
    public void getRestricoes200(){

        String url = "http://localhost:8080/api/v1/restricoes/97093236014";

        given().
        when().
            get(url).
        then().
            statusCode(200).
            body("mensagem", equalTo("O CPF 97093236014 tem problema"));
    }

    @Test
    public void getRestricoes204(){

        String url = "http://localhost:8080/api/v1/restricoes/99999999999";

        given().
        when().
            get(url).
        then().
            statusCode(204);
    }
}