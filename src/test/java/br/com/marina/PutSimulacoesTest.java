package br.com.marina;

import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

public class PutSimulacoesTest {

    @Test
    public void putSimulacoes200(){

        String url = "http://localhost:8080/api/v1/simulacoes";
        JSONObject simulacao = new JSONObject();
        String cpf = "02540405002";
        String nome = "Marina Alice Schreiner";
        String email = "marina.alice1993@gmail.com";
        int parcelas = 12;
        Boolean seguro = true;

        simulacao.put("cpf", cpf);
        simulacao.put("nome", nome);
        simulacao.put("email", email);
        simulacao.put("valor", 10000);
        simulacao.put("parcelas", parcelas);
        simulacao.put("seguro", seguro);

        given().
            pathParam("cpf", cpf).
            contentType(ContentType.JSON).
            body(simulacao.toString()).
        when().
            put(url + "/{cpf}").
        then().
            statusCode(200);
    }

    @Test
    public void putSimulacoes404(){

        String url = "http://localhost:8080/api/v1/simulacoes";
        JSONObject simulacao = new JSONObject();
        String cpf = "03456876759";
        String nome = "Marina Alice Schreiner";
        String email = "marina.alice1993@gmail.com";
        int parcelas = 12;
        Boolean seguro = true;

        simulacao.put("cpf", cpf);
        simulacao.put("nome", nome);
        simulacao.put("email", email);
        simulacao.put("valor", 10000);
        simulacao.put("parcelas", parcelas);
        simulacao.put("seguro", seguro);

        given().
            pathParam("cpf", cpf).
            contentType(ContentType.JSON).
            body(simulacao.toString()).
        when().
            put(url + "/{cpf}").
        then().
            statusCode(404).
            body("mensagem",is("CPF " + cpf + " não encontrado"));
    }
}
