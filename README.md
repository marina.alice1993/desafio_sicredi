# Desafio Sicredi

*Teste de API*

- Ferramentas utilizadas:
    - Arquitetura: Maven (Versão 4.0.0);
    - Linguagem: Java (Versão 8);
    - IDE: IntelliJ (Versão 11.0.9);
    - Ambiente: JDK (Versão 8.0.2710.9);
    - Biblioteca para testes: REST Assured (Versão 4.3.1);
    - Biblioteca para execução: TestNG (Versão 6.14.13);
    - API utilizada: Aplicação disponibilizada para o teste;
    - Recurso utilizado: Rotas '/restricao' e '/simulacao'.

*Detalhes do código*
- Os scripts de teste estão disponíveis no pacote src/test/java/br.com.marina
  
**Get Restrições**: Tem a finalidade de consultar o CPF informando,
      retornando se ele possui ou não uma restrição;
- getRestricoes200 - Se possui restrição, valida o código retornado da chamada HTTP 200 com a mensagem "O CPF 99999999999 possui restrição".
          *Obs: mensagem do desafio diferente do swagger, foi validada a mensagem "O CPF 99999999999 tem problema".*
- getRestricoes204 - Se não possui restrição, valida o código retornado da chamada HTTP 204.
    
**Post Simulações**: A simulação é um cadastro que fica registrado informações importantes.
  - postSimulacoes201 - Se simulação cadastrada com sucesso, valida o código retornado da chamada HTTP 201.
  - postSimulacoes400 - Se simulação com problema, valida o código retornado da chamada HTTP 400.
  - postSimucacoes409 - Se simulação para um mesmo CPF, valida o código retornado da chamada HTTP 409 com a mensagem "CPF já existe".
  *Obs: mensagem do desafio diferente da aplicação, foi validada a mensagem "CPF duplicado" e era esperado um retorno com código 409 para esse cenário, porém a aplicação está retornando o código 400.*
    
**Put Simulações**: Altera uma simulação já existente, onde o CPF deve ser informado para que a
      alteração possa ser efetuada;
- putSimulacoes200 - Valida a alteração de uma simulação já existente, retornando da chamada HTTP 200.
- putSimulacoes404 - Se o CPF não possui uma simulação, valida o código HTTP 404 com a mensagem "CPF não encontrado".

**Get Consultas**: Lista as simulações cadastradas.
- getConsultaTodas - Valida retorno da lista de simulações cadastradas.

- getConsultaTodas204 - Valida retorno HTTP 204 se não existir simulações cadastradas.

- getConsultaCPF - Valida retorno da simulação previamente cadastrada pelo CPF.

- getConsultaCPF404 - Valida CPF não possui uma simulação, retorno HTTP 404 da chamada.

**Delete Simulações**: Remove uma simulação previamente cadastrada;
- deletesimulacoes204 - Retorna o HTTP Status 204 se simulação for removida com sucesso.
*Obs: Aplicação devolve código 200, entretando no teste pede para validar código 204.*

- deletesimulacoes404 - Retorna o HTTP Status 404 com a mensagem "Simulação não encontrada" se não
  existir a simulação pelo ID informado.
  *Obs: Aplicação não retorna o código esperado.*